<?php
/**
 * Created by PhpStorm.
 * User: Lolle
 * Date: 22.01.2018
 * Time: 12:23
 */


$title = "Reservierungsverwaltung";
include_once 'views/layouts/top.php';
?>

    <div class="container">
        <h2><?= $title ?></h2>
        <h3>Herzlich Willkommen bei der Reservierungsverwaltung!</h3>
        <p>Bitte wählen Sie den gewünschten Bereich:</p>
        <a class="btn btn-primary" href="views/room/index.php">Zimmer</a>
        <a class="btn btn-primary" href="views/guest/index.php">G&auml;ste</a>
        <a class="btn btn-primary" href="views/booking/index.php">Reservierungen</a>
    </div>

<?php
include_once "views/layouts/bottom.php";
?>