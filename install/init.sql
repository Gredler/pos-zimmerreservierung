-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema zimmerreservierung_gb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema zimmerreservierung_gb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `zimmerreservierung_gb` DEFAULT CHARACTER SET utf8 ;
USE `zimmerreservierung_gb` ;

-- -----------------------------------------------------
-- Table `zimmerreservierung_gb`.`tbl_guest`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zimmerreservierung_gb`.`tbl_guest` (
  `g_id` INT NOT NULL AUTO_INCREMENT,
  `g_firstname` VARCHAR(100) NOT NULL,
  `g_lastname` VARCHAR(100) NOT NULL,
  `g_email` VARCHAR(100) NOT NULL,
  `g_phone` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`g_id`),
  UNIQUE INDEX `g_email_UNIQUE` (`g_email` ASC))
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zimmerreservierung_gb`.`tbl_room`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zimmerreservierung_gb`.`tbl_room` (
  `r_id` INT NOT NULL AUTO_INCREMENT,
  `r_number` INT NOT NULL,
  `r_name` VARCHAR(500) NOT NULL,
  `r_maxoccupancy` INT NOT NULL,
  `r_price` DOUBLE NOT NULL,
  `r_balcony` TINYINT NOT NULL,
  PRIMARY KEY (`r_id`),
  UNIQUE INDEX `r_number_UNIQUE` (`r_number` ASC))
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zimmerreservierung_gb`.`tbl_booking`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zimmerreservierung_gb`.`tbl_booking` (
  `b_id` INT NOT NULL AUTO_INCREMENT,
  `b_startdate` DATE NOT NULL,
  `b_enddate` DATE NOT NULL,
  `b_paid` TINYINT NOT NULL,
  `tbl_guest_g_id` INT NOT NULL,
  `tbl_room_r_id` INT NOT NULL,
  PRIMARY KEY (`b_id`),
  INDEX `fk_tbl_booking_tbl_guest_idx` (`tbl_guest_g_id` ASC),
  INDEX `fk_tbl_booking_tbl_room1_idx` (`tbl_room_r_id` ASC),
  CONSTRAINT `fk_tbl_booking_tbl_guest`
  FOREIGN KEY (`tbl_guest_g_id`)
  REFERENCES `zimmerreservierung_gb`.`tbl_guest` (`g_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_booking_tbl_room1`
  FOREIGN KEY (`tbl_room_r_id`)
  REFERENCES `zimmerreservierung_gb`.`tbl_room` (`r_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `zimmerreservierung_gb`.`tbl_guest`
-- -----------------------------------------------------
START TRANSACTION;
USE `zimmerreservierung_gb`;
INSERT INTO `zimmerreservierung_gb`.`tbl_guest` (`g_id`, `g_firstname`, `g_lastname`, `g_email`, `g_phone`) VALUES (1, 'Max', 'Mustermann', 'max@worldsdyouroyster.com', '902349290');
INSERT INTO `zimmerreservierung_gb`.`tbl_guest` (`g_id`, `g_firstname`, `g_lastname`, `g_email`, `g_phone`) VALUES (2, 'Testti', 'Test', 'test@testi.test', '666666666');
INSERT INTO `zimmerreservierung_gb`.`tbl_guest` (`g_id`, `g_firstname`, `g_lastname`, `g_email`, `g_phone`) VALUES (3, 'Martin', 'Martini', 'mamartin@martini.com', '002300320');
INSERT INTO `zimmerreservierung_gb`.`tbl_guest` (`g_id`, `g_firstname`, `g_lastname`, `g_email`, `g_phone`) VALUES (4, 'Jean-Luc', 'Picard', 'picard@starfleet.org', '111111111');
INSERT INTO `zimmerreservierung_gb`.`tbl_guest` (`g_id`, `g_firstname`, `g_lastname`, `g_email`, `g_phone`) VALUES (5, 'River', 'Tam', 'tam@firefly.org', '2390482092308429');

COMMIT;


-- -----------------------------------------------------
-- Data for table `zimmerreservierung_gb`.`tbl_room`
-- -----------------------------------------------------
START TRANSACTION;
USE `zimmerreservierung_gb`;
INSERT INTO `zimmerreservierung_gb`.`tbl_room` (`r_id`, `r_number`, `r_name`, `r_maxoccupancy`, `r_price`, `r_balcony`) VALUES (1, 01, 'Tres-Zap', 4, 53.72, 0);
INSERT INTO `zimmerreservierung_gb`.`tbl_room` (`r_id`, `r_number`, `r_name`, `r_maxoccupancy`, `r_price`, `r_balcony`) VALUES (2, 02, 'Pannier', 2, 53.72, 0);
INSERT INTO `zimmerreservierung_gb`.`tbl_room` (`r_id`, `r_number`, `r_name`, `r_maxoccupancy`, `r_price`, `r_balcony`) VALUES (3, 03, 'Gembucket', 1, 81.72, 1);
INSERT INTO `zimmerreservierung_gb`.`tbl_room` (`r_id`, `r_number`, `r_name`, `r_maxoccupancy`, `r_price`, `r_balcony`) VALUES (4, 04, 'Konklab', 4, 99.81, 0);
INSERT INTO `zimmerreservierung_gb`.`tbl_room` (`r_id`, `r_number`, `r_name`, `r_maxoccupancy`, `r_price`, `r_balcony`) VALUES (5, 05, 'Zontrax', 1, 77.96, 0);
INSERT INTO `zimmerreservierung_gb`.`tbl_room` (`r_id`, `r_number`, `r_name`, `r_maxoccupancy`, `r_price`, `r_balcony`) VALUES (6, 06, 'Veribet', 1, 85.50, 0);
INSERT INTO `zimmerreservierung_gb`.`tbl_room` (`r_id`, `r_number`, `r_name`, `r_maxoccupancy`, `r_price`, `r_balcony`) VALUES (7, 07, 'Zoolab', 1, 36.51, 0);
INSERT INTO `zimmerreservierung_gb`.`tbl_room` (`r_id`, `r_number`, `r_name`, `r_maxoccupancy`, `r_price`, `r_balcony`) VALUES (8, 08, 'Keylex', 3, 64.09, 0);
INSERT INTO `zimmerreservierung_gb`.`tbl_room` (`r_id`, `r_number`, `r_name`, `r_maxoccupancy`, `r_price`, `r_balcony`) VALUES (9, 09, 'Regrant', 2, 43.35, 0);
INSERT INTO `zimmerreservierung_gb`.`tbl_room` (`r_id`, `r_number`, `r_name`, `r_maxoccupancy`, `r_price`, `r_balcony`) VALUES (10, 10, 'Namfix', 3, 93.03, 1);

COMMIT;


-- -----------------------------------------------------
-- Data for table `zimmerreservierung_gb`.`tbl_booking`
-- -----------------------------------------------------
START TRANSACTION;
USE `zimmerreservierung_gb`;
INSERT INTO `zimmerreservierung_gb`.`tbl_booking` (`b_id`, `b_startdate`, `b_enddate`, `b_paid`, `tbl_guest_g_id`, `tbl_room_r_id`) VALUES (1, '2018-01-20', '2018-02-01', 1, 1, 3);
INSERT INTO `zimmerreservierung_gb`.`tbl_booking` (`b_id`, `b_startdate`, `b_enddate`, `b_paid`, `tbl_guest_g_id`, `tbl_room_r_id`) VALUES (2, '2018-02-17', '2018-03-15', 0, 3, 5);
INSERT INTO `zimmerreservierung_gb`.`tbl_booking` (`b_id`, `b_startdate`, `b_enddate`, `b_paid`, `tbl_guest_g_id`, `tbl_room_r_id`) VALUES (3, '2019-01-01', '2019-12-31', 1, 4, 6);
INSERT INTO `zimmerreservierung_gb`.`tbl_booking` (`b_id`, `b_startdate`, `b_enddate`, `b_paid`, `tbl_guest_g_id`, `tbl_room_r_id`) VALUES (4, '2018-04-11', '2018-06-08', 0, 2, 2);
INSERT INTO `zimmerreservierung_gb`.`tbl_booking` (`b_id`, `b_startdate`, `b_enddate`, `b_paid`, `tbl_guest_g_id`, `tbl_room_r_id`) VALUES (5, '2017-04-05', '2017-11-01', 1, 5, 1);

COMMIT;

