<?php
/**
 * Created by PhpStorm.
 * User: Andi
 * Date: 19.01.2018
 * Time: 14:31
 */

require_once "DatabaseObject.php";

class Booking implements DatabaseObject
{
    private $id;
    private $guest;
    private $room;
    private $startDate;
    private $endDate;
    private $paid;
    private $errors = [];

    /**
     * Booking constructor.
     * @param $id
     * @param $guest
     * @param $room
     * @param $startDate
     * @param $endDate
     * @param $paid
     */
    public function __construct($id, $guest, $room, $startDate, $endDate, $paid)
    {
        $this->id = $id;
        $this->guest = $guest;
        $this->room = $room;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
        $this->paid = $paid;
    }

    //#############################################################################################
    //######################  Getters & Setters                ####################################
    //#############################################################################################

    /**
     * Getter for some private attributes
     * @return mixed $property
     */
    public function __get($property)
    {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
        return null;
    }

    /**
     * Setter for some private attributes
     * @return mixed $name
     * @return mixed $value
     */
    public function __set($property, $value)
    {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

    /**
     * Set error variable
     * @param $key the position of the error message in errors array
     * @param $message message to be displayed in case of error
     */
    public function setError($key, $message)
    {
        $this->errors[$key] = $message;
    }

    /**
     * Unset error in errors array on given position
     * @param $key the position in the errors array
     */
    public function unsetError($key)
    {
        unset($this->errors[$key]);
    }

    //#############################################################################################
    //######################  CRUD Operations                  ####################################
    //#############################################################################################

    /**
     * Creates a new entry in the database
     * @return integer generated ID of this database entry
     */
    public function create()
    {
        $db = Database::connect();
        $sql = "INSERT INTO tbl_booking (b_startdate, b_enddate, b_paid, tbl_guest_g_id, tbl_room_r_id) 
                VALUES (?, ?, ?, ?, ?)";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($this->startDate, $this->endDate, $this->paid, $this->guest->id, $this->room->id));
        $lastId = $db->lastInsertId();
        Database::disconnect();
        return $lastId;
    }

    /**
     * Update an existing entry in the database
     * @return boolean true on success
     */
    public function update()
    {
        $db = Database::connect();
        $sql = "UPDATE tbl_booking SET b_startdate = ?, b_enddate = ?, b_paid = ?, 
                tbl_guest_g_id = ?, tbl_room_r_id = ? WHERE b_id = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($this->startDate, $this->endDate, $this->paid, $this->guest->id, $this->room->id, $this->id));
        Database::disconnect();
    }

    /**
     * Get an entry from database
     * @param int $id ID of the entry
     * @return Booking|null a Booking object with given id or null if this id cannot be found in database
     */
    public static function get($id)
    {
        $db = Database::connect();
        $sql = "SELECT * from tbl_booking WHERE b_id = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($id));
        $data = $stmt->fetch(PDO::FETCH_ASSOC);
        Database::disconnect();

        if ($data != null) {
            return new Booking($data['b_id'], Guest::get($data['tbl_guest_g_id']), Room::get($data['tbl_room_r_id']),
                $data['b_startdate'], $data['b_enddate'], $data['b_paid']);
        } else {
            return null;
        }
    }

    /**
     * Get all Booking entries in database
     * @return array array of Booking-Objects or empty array if there are no entries
     */
    public static function getAll()
    {
        $bookings = [];
        $db = Database::connect();
        $sql = "select * from tbl_booking inner join tbl_guest on tbl_guest_g_id = g_id inner join tbl_room on tbl_room_r_id = r_id;";
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $data = $stmt->fetchAll();
        Database::disconnect();

        foreach ($data as $booking) {
            $bookings[] = new Booking($booking['b_id'],
                new Guest($booking['g_id'], $booking['g_firstname'], $booking['g_lastname'], $booking['g_email'], $booking['g_phone']),
                new Room($booking['r_id'], $booking['r_number'], $booking['r_name'], $booking['r_maxoccupancy'], $booking['r_price'], $booking['r_balcony']),
                $booking['b_startdate'], $booking['b_enddate'], $booking['b_paid']);
        }
        return $bookings;
    }

    /**
     * Deletes an entry from the database
     * @param integer $id the ID of the entry to be deleted
     */
    public static function delete($id)
    {
        $db = Database::connect();
        $sql = "DELETE FROM tbl_booking WHERE b_id = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($id));
        Database::disconnect();
    }

    /**
     * Queries all booking entries in database which startDate or endDate lies between startDate and endDate
     * of new booking AND which have the same room number as the new booking
     * @return bool true, if database returns no data (that is, when there are no booking collisions), else false
     */
    public function detectCollisions()
    {
        $db = Database::connect();
        $sql = "select * from tbl_booking inner join tbl_guest on tbl_guest_g_id = g_id 
            inner join tbl_room on tbl_room_r_id = r_id 
            where r_id = ? 
            and (b_startdate between ? and ?
            or b_enddate between ? and ?)
            and b_id <> ?;";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($this->room->id, $this->startDate, $this->endDate, $this->startDate, $this->endDate, $this->id));
        $data = $stmt->fetchAll();
        Database::disconnect();

        if (!empty($data)) {
            return true;
        }

        return false;
    }

    //#############################################################################################
    //######################  Custom Methods                   ####################################
    //#############################################################################################

    /**
     * Validates all data from a Booking Object
     * @return bool true, if data is valid, else false
     */
    public function validate()
    {
        return $this->validateStartDate()
            & $this->validateEndDate()
            & HelperClass::validateBooleanHelper($this, $this->paid, "paid")
            & $this->validateGuest()
            & $this->validateRoom();
    }

    /**
     * Checks if the Guest exists in database
     * @return bool true, if the Guest exists, else false
     */
    public function validateGuest()
    {
        if ($this->guest != null) {
            unset($this->errors['guest']);
            return true;
        }
        $this->errors['guest'] = "Gast existiert nicht, bitte überprüfen Sie die Eingabe.";
        return false;
    }

    /**
     * Checks if a room exists in database, and if it is already booked in
     * the specified time (startDate to endDate)
     * @return bool true, if the room exists and there are no bookings in the specified time,
     *              else false
     */
    public function validateRoom()
    {
        if ($this->room != null) {
            if ($this->detectCollisions()) {
                $this->errors['room'] = "Zimmer ist zu dieser Zeit bereits belegt.";
                return false;
            }

            unset($this->errors['room']);
            return true;
        }
        $this->errors['room'] = "Zimmer existiert nicht, bitte überprüfen Sie die Eingabe.";
        return false;
    }

    /**
     * Validates startDate of booking.
     * In case of creating a new entry the startDate has to be today or later
     * In case of updating an existing entry the startDate may be changed
     * to today or later, but only if original startDate is today or later.
     * If original date lies in the past it cannot be changed (as guest has already checked in!)
     * In each case the startDate has to be set
     * @return bool true, if the startDate is valid, else false
     */
    public function validateStartDate()
    {
        if ($this->startDate != null) {
            if ($this->id == null || $this->id <= 0) {
                if ($this->startDate >= date("Y-m-d", time())) {
                    unset($this->errors['startDate']);
                    return true;
                }
                $this->errors['startDate'] = "Anreisedatum darf nicht in der Vergangenheit liegen";
                return false;
            } else {

                $booking = Booking::get($this->id);

                if ($booking !== null) {

                    if ($booking->startDate < date("Y-m-d", time())) {
                        $this->startDate = $booking->startDate;
                        unset($this->errors['startDate']);
                        return true;
                    } else {
                        if ($this->startDate >= date("Y-m-d", time())) {

                            unset($this->errors['startDate']);
                            return true;
                        }
                        $this->errors['startDate'] = "Anreisedatum darf nicht in der Vergangenheit liegen";
                        return false;
                    }
                }
                $this->errors['startDate'] = "Ein Fehler ist aufgetreten, bitte versuchen Sie es erneut.";
                return false;

            }
        }
        $this->errors['startDate'] = "Bitte Anreisedatum angeben";
        return false;
    }

    /**
     * Validates the endDate of a booking
     * Checks, if the endDate is set & is higher than the startDate
     * @return bool true, if the endDate is valid, else false
     */
    public function validateEndDate()
    {

        if ($this->endDate != null) {
            if ($this->endDate > $this->startDate) {
                unset($this->errors['endDate']);
                return true;
            }
            $this->errors['endDate'] = "Abreisedatum muss nach Anreisedatum liegen.";
            return false;
        }
        $this->errors['endDate'] = "Bitte Abreisedatum angeben";
        return false;
    }
}