<?php

class Database
{
    private static $dbName = 'zimmerreservierung_gb';
    private static $dbHost = 'localhost';
    private static $dbUsername = 'root';
    private static $dbUserPassword = '';

    private static $conn = null;

    public function __construct()
    {
        exit('Init function is not allowed');
    }

    public static function connect()
    {
        // One connection through whole application
        if (null == self::$conn) {
            try {
                self::$conn = new PDO("mysql:host=" . self::$dbHost . ";" . "dbname=" . self::$dbName, self::$dbUsername, self::$dbUserPassword);
            } catch (PDOException $e) {
                die($e->getMessage());
            }
        }
        self::$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return self::$conn;
    }

    public static function disconnect()
    {
        self::$conn = null;
    }

    public static function install()
    {
        $conn = new mysqli(self::$dbHost, self::$dbUsername, self::$dbUserPassword);
        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }

        // Create database
        $sql = "CREATE DATABASE " . self::$dbName;
        if ($conn->query($sql) === TRUE) {
            echo "Datenbank erfolgreich erstellt.";
        } else {
            echo "Ein Fehler ist bei der Erstellung der Datenbank aufgetreten: " . $conn->error;
        }

        $conn->close();

        $sql = file_get_contents("init.sql", FILE_USE_INCLUDE_PATH);
        $db = Database::connect();
        $stmt = $db->prepare($sql);
        $stmt->execute();
        Database::disconnect();
    }
}

?>