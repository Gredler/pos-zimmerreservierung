<?php
/**
 * Created by PhpStorm.
 * User: Andi
 * Date: 19.01.2018
 * Time: 14:31
 */

require_once "DatabaseObject.php";
require_once "HelperClass.php";

class Guest implements DatabaseObject
{
    private $id;
    private $firstname;
    private $lastname;
    private $email;
    private $phone;
    private $errors = [];

    /**
     * Guest constructor.
     * @param $id
     * @param $firstname
     * @param $lastname
     * @param $email
     * @param $phone
     */
    public function __construct($id, $firstname, $lastname, $email, $phone)
    {
        $this->id = $id;
        $this->firstname = $firstname;
        $this->lastname = $lastname;
        $this->email = $email;
        $this->phone = $phone;
    }

    //#############################################################################################
    //######################  Getters & Setters                ####################################
    //#############################################################################################

    /**
     * Getter for all private attributes
     * @return mixed $property
     */
    public function __get($property)
    {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
        return null;
    }

    /**
     * Setter for all private attributes
     * @return mixed $name
     * @return mixed $value
     */
    public function __set($property, $value)
    {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

    /**
     * Set error variables
     * @param $key the position of the error message in errors array
     * @param $message message to be displayed in case of error
     */
    public function setError($key, $message){
        $this->errors[$key] = $message;
    }

    /**
     * Unset error in errors array on given position
     * @param $key the position in the errors array
     */
    public function unsetError($key){
        unset($this->errors[$key]);
    }

    //#############################################################################################
    //######################  CRUD Operations                  ####################################
    //#############################################################################################

    /**
     * Creates a new entry in database
     * @return integer generated ID of this database entry
     */
    public function create()
    {
        $db = Database::connect();
        $sql = "INSERT INTO tbl_guest (g_firstname, g_lastname, g_email, g_phone) VALUES (?, ?, ?, ?)";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($this->firstname, $this->lastname, $this->email, $this->phone));
        $lastId = $db->lastInsertId();
        Database::disconnect();
        return $lastId;
    }

    /**
     * Update an existing entry in database
     * @return boolean true on success
     */
    public function update()
    {
        $db = Database::connect();
        $sql = "UPDATE tbl_guest SET g_firstname = ?, g_lastname = ?, g_email = ?, g_phone = ? WHERE g_id = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($this->firstname, $this->lastname, $this->email, $this->phone, $this->id));
        Database::disconnect();
    }

    /**
     * Get an entry from database
     * @param int $id ID of the entry
     * @return Guest a Guest object with given id or null if this id cannot be found in database
     */
    public static function get($id)
    {
        $db = Database::connect();
        $sql = "SELECT * from tbl_guest WHERE g_id = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($id));
        $data = $stmt->fetch(PDO::FETCH_ASSOC);
        Database::disconnect();

        if ($data != null) {
            return new Guest($data['g_id'], $data['g_firstname'], $data['g_lastname'], $data['g_email'], $data['g_phone']);
        } else {
            return null;
        }
    }

    /**
     * Get all Guest entries in database
     * @return array array of Guest-Objects or empty array if there are no entries
     */
    public static function getAll()
    {
        $guests = [];
        $db = Database::connect();
        $sql = "SELECT * from tbl_guest";
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $data = $stmt->fetchAll();
        Database::disconnect();

        foreach ($data as $guest) {
            $guests[] = new Guest($guest['g_id'], $guest['g_firstname'], $guest['g_lastname'], $guest['g_email'], $guest['g_phone']);
        }
        return $guests;
    }

    /**
     * Deletes an entry from the database
     * @param integer $id the ID of the entry to be deleted
     */
    public static function delete($id)
    {
        $db = Database::connect();
        $sql = "DELETE FROM tbl_guest WHERE g_id = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($id));
        Database::disconnect();
    }

    //#############################################################################################
    //######################  Custom Methods                   ####################################
    //#############################################################################################

    /**
     * Validates all data from a Guest Object
     * @return bool true, if data is valid, else false
     */
    public function validate()
    {
        return  HelperClass::validateStrLengthHelper($this, "Vorname", "firstname", $this->firstname, 100, 1 )
            & HelperClass::validateStrLengthHelper($this, "Nachname", "lastname", $this->lastname, 100, 1 )
            & HelperClass::validateStrLengthHelper($this, "E-Mail", "email", $this->email, 100, 1 )
            & HelperClass::validateStrLengthHelper($this, "Telefonnummer", "phone", $this->phone, 100, 1 );
    }

}