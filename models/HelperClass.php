<?php
/**
 * Created by PhpStorm.
 * User: Lolle
 * Date: 22.01.2018
 * Time: 10:55
 */

class HelperClass
{

    /**
     * Validates length of a string.
     * Sets/unsets error variable in case of invalid/valid data
     * @param $object the object holding the field to be validated
     * @param $label name of the input field displayed in error message
     * @param $key position in errors array
     * @param $value the acutal value to be validated
     * @param $maxLength the allowed maximum length
     * @return bool true, if value meets parameters, else false
     */
    public static function validateStrLengthHelper($object, $label, $key, $value, $maxLength)
    {
        if (strlen($value) == 0) {
            $object->setError($key, "$label darf nicht leer sein");
            return false;
        } else if (strlen($value) > $maxLength) {
            $object->setError($key, "$label zu lang (max. $maxLength Zeichen)");
            return false;
        } else {
            $object->unsetError($key);
            return true;
        }
    }

    /**
     * Validates Value of a number.
     * Sets/unsets error variable in case of invalid/valid data
     * @param $object the object holding the field to be validated
     * @param $label name of the input field displayed in error message
     * @param $key position in errors array
     * @param $value the acutal value to be validated
     * @param $maxValue the allowed maximum value
     * @param $minValue the required minimum value
     * @return bool true, if value meets parameters, else false
     */
    public static function validateNumberValueHelper($object, $label, $key, $value, $maxValue, $minValue)
    {
        if ($value > $maxValue) {
            $object->setError($key, "$label zu groß, nur Werte zw. $minValue und $maxValue erlaubt.");
            return false;
        } else if ($value < $minValue) {
            $object->setError($key, "$label zu klein, nur Werte zw. $minValue und $maxValue erlaubt.");
            return false;
        } else {
            $object->unsetError($key);
            return true;
        }
    }


    /**
     * Checks if the boolean property is set
     * @param $object The object to be validated
     * @param $property The property to be validated
     * @param $key The position in the errors array
     * @return bool True if data is valid, else false
     */
    public static function validateBooleanHelper($object, $property, $key)
    {
        if ($property === null) {
            $object->setError($key, "Ein Fehler ist aufgetreten, bitte versuchen Sie es erneut");
            return false;
        } else {
            $object->unsetError($key);
            return true;
        }
    }

    /**
     * Saves entry in database
     * @param $object the object to be saved in database
     * @return bool true if saving was successful, else false
     */
    public static function save($object)
    {
        if ($object->validate()) {
            if($object->id != null || $object->id > 0){
                $object->update();
            } else {
                $object->id = $object->create();
            }
            return true;
        }
        return false;
    }
}