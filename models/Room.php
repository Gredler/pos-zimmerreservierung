<?php
/**
 * Created by PhpStorm.
 * User: Andi
 * Date: 19.01.2018
 * Time: 14:31
 */

require_once "DatabaseObject.php";
require_once "HelperClass.php";

class Room implements DatabaseObject
{
    private $id;
    private $number;
    private $name;
    private $max_occupancy;
    private $price;
    private $balcony;
    private $errors;

    /**
     * Room constructor.
     * @param $id
     * @param $name
     * @param $max_occupancy
     * @param $price
     * @param $balcony
     */
    public function __construct($id, $number, $name, $max_occupancy, $price, $balcony)
    {
        $this->id = $id;
        $this->number = $number;
        $this->name = $name;
        $this->max_occupancy = $max_occupancy;
        $this->price = $price;
        $this->balcony = $balcony;
    }

    //#############################################################################################
    //######################    Getters & Setters              ####################################
    //#############################################################################################

    /**
     * Getter for some private attributes
     * @return mixed $property
     */
    public function __get($property)
    {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
        return null;
    }

    /**
     * Setter for some private attributes
     * @return mixed $name
     * @return mixed $value
     */
    public function __set($property, $value)
    {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

    public function setError($key, $message)
    {
        $this->errors[$key] = $message;
    }

    public function unsetError($key)
    {
        unset($this->errors[$key]);
    }

    //#############################################################################################
    //######################  CRUD Operations                  ####################################
    //#############################################################################################

    /**
     * Creates a new entry in database
     * @return integer generated ID of this database entry
     */
    public function create()
    {
        $db = Database::connect();
        $sql = "INSERT INTO tbl_room (r_number, r_name, r_maxoccupancy, r_price, r_balcony) VALUES (?, ?, ?, ?, ?)";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($this->number, $this->name, $this->max_occupancy, $this->price, $this->balcony));
        $lastId = $db->lastInsertId();
        Database::disconnect();
        return $lastId;
    }

    /**
     * Update an existing object in the database
     * @return boolean true on success
     */
    public function update()
    {
        $db = Database::connect();
        $sql = "UPDATE tbl_room SET r_number = ?, r_name = ?, r_maxoccupancy = ?, r_price = ?, r_balcony = ? WHERE r_id = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($this->number, $this->name, $this->max_occupancy, $this->price, $this->balcony, $this->id));
        Database::disconnect();
    }

    /**
     * Get an object from database
     * @param integer $id
     * @return object single object or null
     */
    public static function get($id)
    {
        $db = Database::connect();
        $sql = "SELECT * from tbl_room WHERE r_id = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($id));
        $data = $stmt->fetch(PDO::FETCH_ASSOC);
        Database::disconnect();

        if ($data != null) {
            return new Room($data['r_id'], $data['r_number'], $data['r_name'], $data['r_maxoccupancy'], $data['r_price'], $data['r_balcony']);
        } else {
            return null;
        }
    }

    /**
     * Get an array of objects from database
     * @return array array of objects or empty array
     */
    public static function getAll()
    {
        $rooms = [];
        $db = Database::connect();
        $sql = "SELECT * from tbl_room";
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $data = $stmt->fetchAll();
        Database::disconnect();

        foreach ($data as $room) {
            $rooms[] = new Room($room['r_id'], $room['r_number'], $room['r_name'], $room['r_maxoccupancy'], $room['r_price'], $room['r_balcony']);
        }
        return $rooms;
    }

    /**
     * Deletes the object from the database
     * @param integer $id
     */
    public static function delete($id)
    {
        $db = Database::connect();
        $sql = "DELETE FROM tbl_room WHERE r_id = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($id));
        Database::disconnect();
    }

    /**
     * Queries all rooms where room number corresponds to this object and id does not
     * @return bool true if there is a collision else false
     */
    public function detectRoomCollisions()
    {
        $db = Database::connect();
        $sql = "SELECT * FROM tbl_room WHERE r_number = ? and r_id <> ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($this->number, $this->id));
        $data = $stmt->fetchAll();
        Database::disconnect();

        if (!empty($data)) {
            return true;
        }

        return false;
    }

    //#############################################################################################
    //######################  Custom Methods                   ####################################
    //#############################################################################################

    /**
     * Validates all fields of the room
     * @return bool true if everything is valid, else false
     */
    public function validate()
    {
        return HelperClass::validateNumberValueHelper($this, "Zimmernummer", "number", $this->number, 10000, 1)
            & $this->validateRoomNumber()
            & HelperClass::validateStrLengthHelper($this, "Zimmername", "name", $this->name, 100)
            & HelperClass::validateNumberValueHelper($this, "Maximale Belegung", "maxOccupancy", $this->max_occupancy, 4, 1)
            & HelperClass::validateNumberValueHelper($this, "Preis", "price", $this->price, 300.0, 1.0)
            & HelperClass::validateBooleanHelper($this, $this->balcony, "balcony");
    }

    /**
     * Validates Room Number.
     * Checks if room number and id-combination exists already
     * @return bool True if Room number is valid, else false
     */
    private function validateRoomNumber()
    {
        if ($this->detectRoomCollisions()) {
            $this->setError('existingRoom', "Ein Zimmer mit dieser Nummer existiert bereits");
            return false;
        }

        return true;
    }
}