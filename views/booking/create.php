<?php
/**
 * Created by PhpStorm.
 * User: Lolle
 * Date: 22.01.2018
 * Time: 12:24
 */

$title = "Reservierung erstellen";
include '../layouts/top.php';
include_once "../../models/Room.php";
include_once "../../models/Guest.php";
include_once "../../models/Booking.php";
require_once "../../models/HelperClass.php";

$booking = new Booking("", "", "", "", "", "");

if (isset($_POST['create'])) {
    $id = 0;
    $guest = isset($_POST['guest']) ? Guest::get($_POST['guest']) : null;
    $room = isset($_POST['room']) ? Room::get($_POST['room']) : null;
    $startDate = isset($_POST['startDate']) ? $_POST['startDate'] : null;
    $endDate = isset($_POST['endDate']) ? $_POST['endDate'] : null;
    $paid = isset($_POST['paid']) ? true : false;

    $booking = new Booking($id, $guest, $room, $startDate, $endDate, $paid);

    if ($booking->validate()) {
        HelperClass::save($booking);
        header("Location: view.php?id=" . $booking->id);
        exit();
    }
}

?>

    <div class="container">
        <div class="row">
            <h2><?= $title ?></h2>
        </div>

        <form class="form-horizontal" action="create.php?" method="post">

            <div class="row">
                <div class="col-md-3">
                    <div class="form-group required ">
                        <label class="control-label">Gast-ID *</label>
                        <input type="number" class="form-control" name="guest" min="0" required="required" value="<?= $booking->guest->id ?>">
                        <?php
                        if(isset($booking->errors['guest'])){
                            echo '<p class="error">' . $booking->errors['guest'] . '</p>';
                        }
                        ?>
                    </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-3">
                    <div class="form-group required ">
                        <label class="control-label">Raum-ID *</label>
                        <input type="number" class="form-control" name="room" min="0" required="required" value="<?= $booking->room->id ?>">
                        <?php
                        if(isset($booking->errors['room'])){
                            echo '<p class="error">' . $booking->errors['room'] . '</p>';
                        }
                        ?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3">
                    <div class="form-group required ">
                        <label class="control-label">Anreise *</label>
                        <input type="date" class="form-control" name="startDate" required="required" value="<?= $booking->startDate ?>">
                        <?php
                        if(isset($booking->errors['startDate'])){
                            echo '<p class="error">' . $booking->errors['startDate'] . '</p>';
                        }
                        ?>
                    </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-3">
                    <div class="form-group required ">
                        <label class="control-label">Abreise *</label>
                        <input type="date" class="form-control" name="endDate" required="required" value="<?= $booking->endDate ?>">
                        <?php
                        if(isset($booking->errors['endDate'])){
                            echo '<p class="error">' . $booking->errors['endDate'] . '</p>';
                        }
                        ?>
                    </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-1">
                    <div class="form-group required ">
                        <label class="control-label">Bezahlt</label>
                        <input type="checkbox" class="form-control-checkbox" name="paid" <?php
                        if ($booking->paid == true) {
                            echo 'checked';
                        }
                        ?>>
                    </div>
                </div>
                <div class="col-md-5"></div>
            </div>

            <div class="form-group">
                <button type="submit" name="create" class="btn btn-success">Erstellen</button>
                <a class="btn btn-default" href="index.php">Abbruch</a>
            </div>
        </form>

    </div> <!-- /container -->

<?php
include '../layouts/bottom.php';
?>