<?php
/**
 * Created by PhpStorm.
 * User: Lolle
 * Date: 22.01.2018
 * Time: 12:24
 */

$title = "Reservierung löschen";
include '../layouts/top.php';
include_once "../../models/Guest.php";
include_once "../../models/Room.php";
include_once "../../models/Booking.php";

include_once "../helper/viewhelper.php";

$booking = Booking::get($_GET['id']);

if ($booking == null) {
    header("Location: index.php");
    exit();
}

if (isset($_POST['delete'])) {
    Booking::delete($_POST['id']);
    header("Location: index.php");
    exit();
}
?>

    <div class="container">
        <h2><?= $title ?></h2>

        <form class="form-horizontal" action="delete.php?id=<?= $booking->id ?>" method="post">
            <input type="hidden" name="id" value="<?= $booking->id ?>"/>
            <p class="alert alert-error">Wollen Sie die Reservierung wirklich löschen?</p>
            <div class="form-actions">
                <button type="submit" class="btn btn-danger" name="delete">Löschen</button>
                <a class="btn btn-default" href="index.php">Abbruch</a>
            </div>
        </form>

    </div> <!-- /container -->

<?php
include '../layouts/bottom.php';
?>