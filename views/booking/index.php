<?php
/**
 * Created by PhpStorm.
 * User: Lolle
 * Date: 22.01.2018
 * Time: 12:22
 */

$title = "Reservierungsverwaltung";
include '../layouts/top.php';

include_once "../../models/Room.php";
include_once "../../models/Guest.php";
include_once "../../models/Booking.php";
?>

    <div class="container">
        <div class="row">
            <h2><?= $title ?></h2>
        </div>
        <div class="row">
            <p>
                <a href="create.php" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span> Erstellen</a>
            </p>

            <table class="table table-striped table-hover table-bordered">
                <thead>
                <tr>
                    <th class="col-md-1">Res-Nr</th>
                    <th class="col-md-3">Zimmer</th>
                    <th class="col-md-3">Gast</th>
                    <th class="col-md-1">Anreise</th>
                    <th class="col-md-1">Abreise</th>
                    <th class="col-md-1">Bezahlt</th>
                    <th class="col-md-2">Optionen</th>
                </tr>
                </thead>
                <tbody>
                <?php

                foreach (Booking::getAll() as $booking) {
                    echo '<tr>';
                    echo '<td class="col-md-1">' . $booking->id . '</td>';
                    echo '<td class="col-md-3">' . $booking->room->number . ', ' . $booking->room->name. '</td>';
                    echo '<td class="col-md-3">' . $booking->guest->lastname . ', ' . $booking->guest->firstname . '</td>';
                    echo '<td class="col-md-1">' . $booking->startDate . '</td>';
                    echo '<td class="col-md-1">' . $booking->endDate . '</td>';
                    echo '<td class="col-md-1">';
                    if ($booking->paid == 1) {
                        echo 'Ja';
                    } else {
                        echo 'Nein';
                    }
                    echo '</td>';
                    echo '<td class="col-md-2">';
                    echo '<a class="btn btn-info" href="view.php?id=' . $booking->id . '">';
                    echo '<span class="glyphicon glyphicon-eye-open"></span></a>&nbsp';
                    echo '<a class="btn btn-primary" href="update.php?id=' . $booking->id . '">';
                    echo '<span class="glyphicon glyphicon-pencil"></span></a>&nbsp;' ;
                    echo '<a class="btn btn-danger" href="delete.php?id=' . $booking->id . '">';
                    echo '<span class="glyphicon glyphicon-remove"></span></a>';
                    echo '</td>';
                    echo '</tr>';
                }

                ?>

                </tbody>
            </table>
        </div>
    </div> <!-- /container -->

<?php
include '../layouts/bottom.php';
?>