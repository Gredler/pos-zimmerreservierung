<?php
/**
 * Created by PhpStorm.
 * User: Lolle
 * Date: 22.01.2018
 * Time: 12:23
 */


$title = "Reservierung anzeigen";
include '../layouts/top.php';
include_once "../../models/Guest.php";
include_once "../../models/Room.php";
include_once "../../models/Booking.php";

include_once "../helper/viewhelper.php";

$booking = Booking::get($_GET['id']);

if ($booking == null) {
    header("Location: index.php");
    exit();
}

?>

    <div class="container">
        <h2><?= $title ?></h2>

        <p>
            <a class="btn btn-primary" href="update.php?id=<?= $booking->id ?>">Aktualisieren</a>
            <a class="btn btn-danger" href="delete.php?id=<?= $booking->id ?>">Löschen</a>
            <a class="btn btn-default" href="index.php">Zurück</a>
        </p>

        <table class="table table-striped table-bordered detail-view">
            <tbody>
            <tr>
                <th class="col-md-3">Reservierungsnummer</th>
                <td class="col-md-9"><?= $booking->id ?></td>
            </tr>
            <tr>
                <th class="col-md-3">Zimmer</th>
                <td class="col-md-9"><?= $booking->room->number . ', ' . $booking->room->name ?></td>
            </tr>
            <tr>
                <th class="col-md-3">Gast</th>
                <td class="col-md-9"><?= $booking->guest->lastname . ', ' . $booking->guest->firstname  ?></td>
            </tr>
            <tr>
                <th class="col-md-3">Anreise</th>
                <td class="col-md-9"><?= $booking->startDate ?></td>
            </tr>
            <tr>
                <th class="col-md-3">Abreise</th>
                <td class="col-md-9"><?= $booking->endDate ?></td>
            </tr>
            <tr>
                <th class="col-md-3">Bezahlt</th>
                <td class="col-md-9"><?php
                    if ($booking->paid == 1) {
                        echo 'Ja';
                    } else {
                        echo 'Nein';
                    }
                    ?></td>
            </tr>
            </tbody>
        </table>
    </div> <!-- /container -->

<?php
include '../layouts/bottom.php';
?>