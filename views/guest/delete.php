<?php
/**
 * Created by PhpStorm.
 * User: Lolle
 * Date: 22.01.2018
 * Time: 12:22
 */

$title = "Gast-Informationen löschen";
include '../layouts/top.php';
include_once "../../models/Guest.php";

include_once "../helper/viewhelper.php";

$guest = Guest::get($_GET['id']);

if ($guest == null) {
    header("Location: index.php");
    exit();
}

if (isset($_POST['delete'])) {
    Guest::delete($_POST['id']);
    header("Location: index.php");
    exit();
}
?>

    <div class="container">
        <h2><?= $title ?></h2>

        <form class="form-horizontal" action="delete.php?id=<?= $guest->id ?>" method="post">
            <input type="hidden" name="id" value="<?= $guest->id ?>"/>
            <p class="alert alert-error">Wollen Sie die Informationen über diesen Gast wirklich löschen?</p>
            <div class="form-actions">
                <button type="submit" class="btn btn-danger" name="delete">Löschen</button>
                <a class="btn btn-default" href="index.php">Abbruch</a>
            </div>
        </form>

    </div> <!-- /container -->

<?php
include '../layouts/bottom.php';
?>