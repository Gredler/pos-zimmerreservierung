<?php
/**
 * Created by PhpStorm.
 * User: Lolle
 * Date: 22.01.2018
 * Time: 12:22
 */

$title = "Gästeverwaltung";
include '../layouts/top.php';
include_once "../../models/Guest.php";
?>

<div class="container">
    <div class="row">
        <h2><?= $title ?></h2>
    </div>
    <div class="row">
        <p>
            <a href="create.php" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span> Erstellen</a>
        </p>

        <table class="table table-striped table-hover table-bordered">
            <thead>
            <tr>
                <th class="col-md-1">ID</th>
                <th class="col-md-2">Vorname</th>
                <th class="col-md-2">Nachname</th>
                <th class="col-md-3">E-Mail</th>
                <th class="col-md-2">Telefonnumer</th>
                <th class="col-md-2">Optionen</th>
            </tr>
            </thead>
            <tbody>
            <?php

            foreach (Guest::getAll() as $guest) {
                echo '<tr>';
                echo '<td class="col-md-1">' . $guest->id . '</td>';
                echo '<td class="col-md-2">' . $guest->firstname . '</td>';
                echo '<td class="col-md-2">' . $guest->lastname . '</td>';
                echo '<td class="col-md-3">' . $guest->email . '</td>';
                echo '<td class="col-md-2">' . $guest->phone . '</td>';
                echo '<td class="col-md-2">';
                echo '<a class="btn btn-info" href="view.php?id=' . $guest->id . '">';
                echo '<span class="glyphicon glyphicon-eye-open"></span></a>&nbsp';
                echo '<a class="btn btn-primary" href="update.php?id=' . $guest->id . '">';
                echo '<span class="glyphicon glyphicon-pencil"></span></a>&nbsp;' ;
                echo '<a class="btn btn-danger" href="delete.php?id=' . $guest->id . '">';
                echo '<span class="glyphicon glyphicon-remove"></span></a>';
                echo '</td>';
                echo '</tr>';
            }

            ?>

            </tbody>
        </table>
    </div>
</div> <!-- /container -->

<?php
include '../layouts/bottom.php';
?>