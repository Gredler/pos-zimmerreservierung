<?php
/**
 * Created by PhpStorm.
 * User: Lolle
 * Date: 22.01.2018
 * Time: 12:23
 */

$title = "Gast bearbeiten";
include '../layouts/top.php';
include_once "../../models/Guest.php";
require_once "../../models/HelperClass.php";

include_once "../helper/viewhelper.php";

$guest = Guest::get($_GET['id']);

if ($guest == null) {
    header("Location: index.php");
    exit();
}

if (isset($_POST['update'])) {
    $id = $_GET['id'];
    $firstname = isset($_POST['firstname']) ? $_POST['firstname'] : null;
    $lastname = isset($_POST['lastname']) ? $_POST['lastname'] : null;
    $email = isset($_POST['email']) ? $_POST['email'] : null;
    $phone = isset($_POST['phone']) ? $_POST['phone'] : null;

    $guest = new Guest($id, $firstname, $lastname, $email, $phone);

    if ($guest->validate()) {
        HelperClass::save($guest);
        header("Location: view.php?id=" . $guest->id);
        exit();
    }
}

?>

    <div class="container">
        <div class="row">
            <h2><?= $title ?></h2>
        </div>

        <form class="form-horizontal" action="update.php?id=<?= $guest->id ?>" method="post">

            <div class="row">
                <div class="col-md-3">
                    <div class="form-group required ">
                        <label class="control-label">Vorname *</label>
                        <input type="text" class="form-control" name="firstname" maxlength="100" required="required" value="<?= $guest->firstname ?>">
                        <?php
                        if (isset($guest->errors['firstname'])) {
                            echo '<p class="error">';
                            echo $guest->errors['firstname'];
                            echo '</p>';
                        }
                        ?>
                    </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-3">
                    <div class="form-group required ">
                        <label class="control-label">Nachname *</label>
                        <input type="text" class="form-control" name="lastname" maxlength="100" required="required" value="<?= $guest->lastname ?>">
                        <?php
                        if (isset($guest->errors['lastname'])) {
                            echo '<p class="error">';
                            echo $guest->errors['lastname'];
                            echo '</p>';
                        }
                        ?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3">
                    <div class="form-group required ">
                        <label class="control-label">E-Mail *</label>
                        <input type="text" class="form-control" name="email" required="required" maxlength="100" value="<?= $guest->email ?>">
                        <?php
                        if (isset($guest->errors['email'])) {
                            echo '<p class="error">';
                            echo $guest->errors['email'];
                            echo '</p>';
                        }
                        ?>
                    </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-3">
                    <div class="form-group required ">
                        <label class="control-label">Telefonnummer *</label>
                        <input type="text" class="form-control" name="phone" required="required" maxlength="100" value="<?= $guest->phone ?>">
                        <?php
                        if (isset($guest->errors['phone'])) {
                            echo '<p class="error">';
                            echo $guest->errors['phone'];
                            echo '</p>';
                        }
                        ?>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary" name="update">Aktualisieren</button>
                <a class="btn btn-default" href="index.php">Abbruch</a>
            </div>
        </form>

    </div> <!-- /container -->

<?php
include '../layouts/bottom.php';
?>