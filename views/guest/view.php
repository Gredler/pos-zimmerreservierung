<?php
/**
 * Created by PhpStorm.
 * User: Lolle
 * Date: 22.01.2018
 * Time: 12:23
 */


$title = "Gast anzeigen";
include '../layouts/top.php';
include_once "../../models/Guest.php";

include_once "../helper/viewhelper.php";

$guest = Guest::get($_GET['id']);

if ($guest == null) {
    header("Location: index.php");
    exit();
}

?>

    <div class="container">
        <h2><?= $title ?></h2>

        <p>
            <a class="btn btn-primary" href="update.php?id=<?= $guest->id ?>">Aktualisieren</a>
            <a class="btn btn-danger" href="delete.php?id=<?= $guest->id ?>">Löschen</a>
            <a class="btn btn-default" href="index.php">Zurück</a>
        </p>

        <table class="table table-striped table-bordered detail-view">
            <tbody>
            <tr>
                <th class="col-md-3">ID</th>
                <td class="col-md-9"><?= $guest->id ?></td>
            </tr>
            <tr>
                <th class="col-md-3">Vorname</th>
                <td class="col-md-9"><?= $guest->firstname ?></td>
            </tr>
            <tr>
                <th class="col-md-3">Nachname</th>
                <td class="col-md-9"><?= $guest->lastname ?></td>
            </tr>
            <tr>
                <th class="col-md-3">E-Mail</th>
                <td class="col-md-9"><?= $guest->email ?></td>
            </tr>
            <tr>
                <th class="col-md-3">Telefonnummer</th>
                <td class="col-md-9"><?= $guest->phone ?></td>
            </tr>
            </tbody>
        </table>
    </div> <!-- /container -->

<?php
include '../layouts/bottom.php';
?>