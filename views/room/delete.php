<?php
$title = "Zimmer löschen";
include '../layouts/top.php';
include_once "../../models/Room.php";

include_once "../helper/viewhelper.php";

$room = Room::get($_GET['id']);

if ($room == null) {
    header("Location: index.php");
    exit();
}

if (isset($_POST['delete'])) {
    Room::delete($_POST['id']);
    header("Location: index.php");
    exit();
}
?>

    <div class="container">
        <h2><?= $title ?></h2>

        <form class="form-horizontal" action="delete.php?id=<?= $room->id ?>" method="post">
            <input type="hidden" name="id" value="<?= $room->id ?>"/>
            <p class="alert alert-error">Wollen Sie das Zimmer wirklich löschen?</p>
            <div class="form-actions">
                <button type="submit" class="btn btn-danger" name="delete">Löschen</button>
                <a class="btn btn-default" href="index.php">Abbruch</a>
            </div>
        </form>

    </div> <!-- /container -->

<?php
include '../layouts/bottom.php';
?>