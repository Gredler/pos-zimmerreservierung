<?php
$title = "Zimmerverwaltung";
include '../layouts/top.php';
include_once "../../models/Room.php";
?>

    <div class="container">
        <div class="row">
            <h2><?= $title ?></h2>
        </div>
        <div class="row">
            <p>
                <a href="create.php" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span> Erstellen</a>
            </p>

            <table class="table table-striped table-hover table-bordered">
                <thead>
                <tr>
                    <th class="col-md-1">Nummer</th>
                    <th class="col-md-6">Name</th>
                    <th class="col-md-1">Personen</th>
                    <th class="col-md-1">Preis</th>
                    <th class="col-md-1">Balkon</th>
                    <th class="col-md-2">Optionen</th>
                </tr>
                </thead>
                <tbody>
                <?php

                foreach (Room::getAll() as $room) {
                    echo '<tr>';
                    echo '<td class="col-md-1">' . $room->number . '</td>';
                    echo '<td class="col-md-6">' . $room->name . '</td>';
                    echo '<td class="col-md-1">' . $room->max_occupancy . '</td>';
                    echo '<td class="col-md-1">&euro;&nbsp;' . $room->price . '</td>';
                    echo '<td class="col-md-1">';
                    if ($room->balcony == 1) {
                        echo 'Ja';
                    } else {
                        echo 'Nein';
                    }
                    echo '</td>';

                    echo '<td class="col-md-2">';
                    echo '<a class="btn btn-info" href="view.php?id=' . $room->id . '">';
                    echo '<span class="glyphicon glyphicon-eye-open"></span></a>&nbsp';
                    echo '<a class="btn btn-primary" href="update.php?id=' . $room->id . '">';
                    echo '<span class="glyphicon glyphicon-pencil"></span></a>&nbsp;' ;
                    echo '<a class="btn btn-danger" href="delete.php?id=' . $room->id . '">';
                    echo '<span class="glyphicon glyphicon-remove"></span></a>';
                    echo '</td>';

                    echo '</tr>';
                }

                ?>

                </tbody>
            </table>
        </div>
    </div> <!-- /container -->

<?php
include '../layouts/bottom.php';
?>