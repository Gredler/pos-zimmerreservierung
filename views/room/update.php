<?php
$title = "Zimmer bearbeiten";
include '../layouts/top.php';
include_once "../../models/Room.php";
require_once "../../models/HelperClass.php";

include_once "../helper/viewhelper.php";

$room = Room::get($_GET['id']);

if ($room == null) {
    header("Location: index.php");
    exit();
}

if (isset($_POST['update'])) {
    $id = $_GET['id'];
    $nr = isset($_POST['nr']) ? $_POST['nr'] : null;
    $name = isset($_POST['name']) ? $_POST['name'] : null;
    $maxOccupancy = isset($_POST['maxOccupancy']) ? $_POST['maxOccupancy'] : null;
    $price = isset($_POST['price']) ? $_POST['price'] : null;
    $balcony = isset($_POST['balcony']) ? true : false;

    $room = new Room($id, $nr, $name, $maxOccupancy, $price, $balcony);

    if ($room->validate()) {
        HelperClass::save($room);
        header("Location: view.php?id=" . $room->id);
        exit();
    }
}

?>

<div class="container">
    <div class="row">
        <h2><?= $title ?></h2>
    </div>

    <form class="form-horizontal" action="update.php?id=<?= $room->id ?>" method="post">

        <div class="row">
            <div class="col-md-2">
                <div class="form-group required ">
                    <label class="control-label">Zimmernummer *</label>
                    <input type="number" class="form-control" name="nr" min="1" max="9999" value="<?= $room->number ?>">
                    <?php
                    if (isset($room->errors['number'])) {
                        echo '<p class="error">';
                        echo $room->errors['number'];
                        echo '</p>';
                    }
                    ?>

                    <?php
                    if (isset($room->errors['existingRoom'])) {
                        echo '<p class="error">';
                        echo $room->errors['existingRoom'];
                        echo '</p>';
                    }
                    ?>
                </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-4">
                <div class="form-group required ">
                    <label class="control-label">Name *</label>
                    <input type="text" class="form-control" name="name" maxlength="100" value="<?= $room->name ?>">
                    <?php
                    if (isset($room->errors['name'])) {
                        echo '<p class="error">';
                        echo $room->errors['name'];
                        echo '</p>';
                    }
                    ?>
                </div>
            </div>
            <div class="col-md-5"></div>
        </div>

        <div class="row">
            <div class="col-md-2">
                <div class="form-group required ">
                    <label class="control-label">Personen *</label>
                    <input type="number" class="form-control" name="maxOccupancy" min="1" max="4" value="<?= $room->max_occupancy ?>">
                    <?php
                    if (isset($room->errors['maxOccupancy'])) {
                        echo '<p class="error">';
                        echo $room->errors['maxOccupancy'];
                        echo '</p>';
                    }
                    ?>
                </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-2">
                <div class="form-group required ">
                    <label class="control-label">Preis *</label>
                    <input type="number" step="0.01" class="form-control" name="price" min="1" max="300" value="<?= $room->price ?>">
                    <?php
                    if (isset($room->errors['price'])) {
                        echo '<p class="error">';
                        echo $room->errors['price'];
                        echo '</p>';
                    }
                    ?>
                </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-1">
                <div class="form-group required ">
                    <label class="control-label">Balkon</label>
                    <input type="checkbox" class="form-control-checkbox" name="balcony" <?php
                    if ($room->balcony == 1) {
                        echo 'checked="checked"';
                    }
                    echo '>';
                    ?>
                </div>
            </div>
            <div class="col-md-5"></div>
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary" name="update">Aktualisieren</button>
            <a class="btn btn-default" href="index.php">Abbruch</a>
        </div>
    </form>

</div> <!-- /container -->

<?php
include '../layouts/bottom.php';
?>
