<?php
$title = "Zimmer anzeigen";
include '../layouts/top.php';
include_once "../../models/Room.php";

include_once "../helper/viewhelper.php";

$room = Room::get($_GET['id']);

if ($room == null) {
    header("Location: index.php");
    exit();
}

?>

    <div class="container">
        <h2><?= $title ?></h2>

        <p>
            <a class="btn btn-primary" href="update.php?id=<?= $room->id ?>">Aktualisieren</a>
            <a class="btn btn-danger" href="delete.php?id=<?= $room->id ?>">Löschen</a>
            <a class="btn btn-default" href="index.php">Zurück</a>
        </p>

        <table class="table table-striped table-bordered detail-view">
            <tbody>
            <tr>
                <th class="col-md-3">Zimmernummer</th>
                <td class="col-md-9"><?= $room->number ?></td>
            </tr>
            <tr>
                <th class="col-md-3">Name</th>
                <td class="col-md-9"><?= $room->name ?></td>
            </tr>
            <tr>
                <th class="col-md-3">Personen</th>
                <td class="col-md-9"><?= $room->max_occupancy ?></td>
            </tr>
            <tr>
                <th class="col-md-3">Preis</th>
                <td class="col-md-9">€&nbsp;<?= $room->price ?></td>
            </tr>
            <tr>
                <th class="col-md-3">Balkon</th>
                <td class="col-md-9"><?php
                    if ($room->balcony == 1) {
                        echo 'Ja';
                    } else {
                        echo 'Nein';
                    }
                    ?></td>
            </tr>
            </tbody>
        </table>
    </div> <!-- /container -->

<?php
include '../layouts/bottom.php';
?>